package errores;

import static analisadorLexico.AnalizadorLexicoPascal.obtenerNroLinea;
import static util.LectorArchivoFuente.numeroLinea;

public class Error {

    private final static String RESALTADOR = "\"";

    public static void errorLexico(String error) {
        System.out.println("Error: " + error + ".");
        System.exit(0);
    }

    public static void errorSintactico(String error) {
        System.out.println("Error en la linea " + numeroLinea() + ": " + error + ".");
        System.exit(0);
    }

    public static void errorSemantico(String error) {
        System.out.println("Error en la linea " + obtenerNroLinea() + ": " + error + ".");
        System.exit(0);
    }

    public static void warning(String error) {
        System.out.println("Warning: En la linea " + obtenerNroLinea()
                + " se encontro el caracter " + error);
        System.exit(0);
    }

    public static String resaltar(String frase) {
        return RESALTADOR + frase + RESALTADOR;
    }
}
