package analisadorSemantico;

import java.util.HashMap;

public class TablaDeSimbolo {

    private String nombre;
    private final HashMap<String, Identificador> contenido;
    private int pos;
    private final int anidamiento;

    public TablaDeSimbolo(int anidamiento) {
        contenido = new HashMap<>();
        pos = 0;
        this.anidamiento = anidamiento;
    }

    public int getAnidamiento() {
        return anidamiento;
    }

    public int cantidadVariables() {
        int cantVariables = 0;
        for (HashMap.Entry<String, Identificador> entry : contenido.entrySet()) {
            if (entry.getValue().getClass() == Entero.class
                    || entry.getValue().getClass() == Booleano.class) {
                cantVariables++;
            }
        }
        return cantVariables;
    }

    public Identificador obtenerId(String valor) {
        return contenido.get(valor);
    }

    public boolean agregar(Identificador id) {
        if (id.getClass() == Booleano.class) {
            ((Booleano) id).desplazamiento = pos;
            pos++;
        } else if (id.getClass() == Entero.class) {
            ((Entero) id).desplazamiento = pos;
            pos++;
        }
        id.setAnidamiento(anidamiento);
        return (contenido.put(id.getNombre(), id) == null);
    }

    public boolean agregar(Primitivo id, int posicion) {
        if (id.getClass() == Booleano.class) {
            ((Booleano) id).desplazamiento = posicion;

        } else if (id.getClass() == Entero.class) {
            ((Entero) id).desplazamiento = posicion;

        }
        id.setAnidamiento(anidamiento);
        return (contenido.put(id.getNombre(), id) == null);
    }

    public boolean existe(String valorId) {
        return contenido.get(valorId) != null;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }
}
