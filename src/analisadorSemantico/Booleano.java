package analisadorSemantico;

public class Booleano extends Primitivo {

    public Booleano(String nombre) {
        super(nombre);
    }

    public Booleano(String nombre, int des) {
        super(nombre, des);
    }

    @Override
    public boolean esBooleano() {
        return true;
    }
}
