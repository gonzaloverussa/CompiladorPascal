package analisadorSemantico;

import java.util.ArrayList;
import static errores.Error.errorSemantico;
import static errores.Error.resaltar;

public class ManejadorDeTablas {

    private final ArrayList<TablaDeSimbolo> pila;

    public ManejadorDeTablas() {
        pila = new ArrayList<>();
    }

    public Identificador chequearExistencia(String valorId) {
        Identificador id = null;
        int longitud = pila.size();
        int i = 0;
        boolean encontrado = false;
        while (i < longitud && !encontrado) {
            encontrado = pila.get(i).existe(valorId);
            i++;
        }
        if (!encontrado) {
            //el identificador no existe en ninguna de las tablas
            errorSemantico("El identificador " + resaltar(valorId)
                    + " no esta definido");
        } else {
            //i-- porque se sumo 1 al i despues de haberlo encontrado
            id = pila.get(i - 1).obtenerId(valorId);
        }
        return id;
    }

    public void insertar(Identificador id) {
        if (!pila.get(0).agregar(id)) {
            errorSemantico("El identificador " + resaltar(id.getNombre())
                    + " ya esta definido");
        }
    }

    public void insertar(Primitivo id, int pos) {
        if (!pila.get(0).agregar(id, pos)) {
            errorSemantico("El identificador " + resaltar(id.getNombre())
                    + " ya esta definido");
        }
    }

    public int obtenerCantidadVariables() {
        return pila.get(0).cantidadVariables();
    }

    public int obtenerPosicion(String nombre) {
        return ((Primitivo) pila.get(0).obtenerId(nombre)).desplazamiento;
    }

    public void insertar(ArrayList<Primitivo> listaVariable) {
        for (Identificador identificador : listaVariable) {
            insertar(identificador);
        }
    }

    public void insertarParametros(ArrayList<Primitivo> listaVariable) {
        int pos = -listaVariable.size() - 2;
        for (Primitivo identificador : listaVariable) {
            insertar(identificador, pos);
            pos++;
        }
    }

    public Identificador obtenerIdLocal(String valor) {
        return pila.get(0).obtenerId(valor);
    }

    public void crearTabla(Procedure proc, String nombre) {
        int anidamiento;
        if (pila.isEmpty()) {
            anidamiento = 0;
        } else {
            anidamiento = pila.get(0).getAnidamiento() + 1;
        }
        TablaDeSimbolo tabla = new TablaDeSimbolo(anidamiento);
        tabla.setNombre(nombre);
        pila.add(0, tabla);
        insertar(proc);
    }

    public int obtenerAnidamiento() {
        return pila.get(0).getAnidamiento();
    }

    public void eliminarTabla() {
        pila.remove(0);
    }

    public boolean esNombreTable(String nombre) {
        return pila.get(0).getNombre().equalsIgnoreCase(nombre);
    }
}
