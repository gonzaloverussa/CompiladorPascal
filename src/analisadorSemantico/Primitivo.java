package analisadorSemantico;

public abstract class Primitivo extends Identificador {

    protected int desplazamiento;

    public Primitivo(String nombre) {
        super(nombre);
    }

    public Primitivo(String nombre, int des) {
        super(nombre);
        desplazamiento = des;
    }

    public boolean esBooleano() {
        return false;
    }

    public boolean esEntero() {
        return false;
    }

    public void setDesplazamiento(int desplazamiento) {
        this.desplazamiento = desplazamiento;
    }

    @Override
    public Primitivo getTipo() {
        return this;
    }

    public int getDesplazamiento() {
        return desplazamiento;
    }

    @Override
    public String toString() {
        return this.nombre;
    }
}
