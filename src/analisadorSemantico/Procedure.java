package analisadorSemantico;

import java.util.ArrayList;
import static errores.Error.errorSemantico;
import static errores.Error.resaltar;

public class Procedure extends Identificador {

    protected String etiqueta;
    protected ArrayList<Primitivo> parametros;

    public Procedure(String nombre, ArrayList<Primitivo> params) {
        super(nombre);
        parametros = params;
    }

    public Procedure(String nombre, ArrayList<Primitivo> params, String etiqueta) {
        super(nombre);
        parametros = params;
        this.etiqueta = etiqueta;
    }

    public void setEtiqueta(String e) {
        this.etiqueta = e;
    }

    public String getEtiqueta() {
        return etiqueta;
    }

    public ArrayList<Primitivo> getParametros() {
        return parametros;
    }

    public void setParametros(ArrayList<Primitivo> parametros) {
        this.parametros = parametros;
    }

    @Override
    public void parametrosCorrectos(ArrayList<Primitivo> parmsRecibidos) {
        boolean igualTipo = true;
        int i = 0;
        String parmEsperados = "", parmRecibidos = "";
        if (parametros.size() == parmsRecibidos.size()) {
            while (igualTipo && i < parametros.size()) {
                igualTipo = parametros.get(i).esMismoTipo(parmsRecibidos.get(i));
                i++;
            }

        } else {
            igualTipo = false;
        }
        if (!igualTipo) {
            for (Primitivo parm : parametros) {
                if (parmEsperados.equals("")) {
                    parmEsperados = parm.toStringTipo();
                } else {
                    parmEsperados += "," + parm.toStringTipo();
                }
            }
            for (Primitivo parm : parmsRecibidos) {
                if (parmRecibidos.equals("")) {
                    parmRecibidos = parm.toStringTipo();
                } else {
                    parmRecibidos += "," + parm.toStringTipo();
                }
            }
            errorSemantico("El procedimiento/funcion " + resaltar(nombre)
                    + " esperaba los parametros " + parmEsperados + " y recibio "
                    + parmRecibidos);
        }
    }
}
