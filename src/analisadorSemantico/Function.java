package analisadorSemantico;

import java.util.ArrayList;

public class Function extends Procedure {

    Primitivo retorno;

    public Function(String nombre, ArrayList<Primitivo> params, Primitivo retorno) {
        super(nombre, params);
        this.retorno = retorno;
    }

    public Primitivo getRetorno() {
        return retorno;
    }

    public void setRetorno(Primitivo retorno) {
        this.retorno = retorno;
    }

    @Override
    public boolean esMismoTipo(Identificador id) {
        return id.getClass() == retorno.getClass();
    }

    @Override
    public Primitivo getTipo() {
        return this.retorno;
    }
}
