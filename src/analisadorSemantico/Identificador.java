package analisadorSemantico;

import java.util.ArrayList;
import static errores.Error.errorSemantico;
import static errores.Error.resaltar;

public abstract class Identificador {

    protected String nombre;
    protected int anidamiento;

    public Identificador(String nombre) {
        this.nombre = nombre;
    }

    public String getNombre() {
        return this.nombre;
    }

    public void parametrosCorrectos(ArrayList<Primitivo> params) {
        errorSemantico("El identificador " + resaltar(nombre) + " no es una "
                + resaltar("funcion") + " o " + resaltar("procedimiento"));
    }

    public boolean esMismoTipo(Identificador id) {
        return id.getClass() == this.getClass();
    }

    public String toStringTipo() {
        return this.getClass().getSimpleName();
    }

    public int getAnidamiento() {
        return anidamiento;
    }

    public void setAnidamiento(int anidamiento) {
        this.anidamiento = anidamiento;
    }

    public Primitivo getTipo() {
        return null;
    }
}
