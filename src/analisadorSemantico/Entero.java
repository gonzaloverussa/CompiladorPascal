package analisadorSemantico;

public class Entero extends Primitivo {

    public Entero(String nombre) {
        super(nombre);
    }

    public Entero(String nombre, int des) {
        super(nombre, des);
    }

    @Override
    public boolean esEntero() {
        return true;
    }
}
