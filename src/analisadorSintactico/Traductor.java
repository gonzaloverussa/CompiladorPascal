package analisadorSintactico;

import analisadorLexico.AnalizadorLexicoPascal;
import analisadorLexico.CreadorTokens;
import analisadorLexico.Token;
import analisadorSemantico.*;
import java.util.ArrayList;
import static errores.Error.errorSintactico;
import static errores.Error.errorSemantico;
import static errores.Error.resaltar;
import generadorDeCodigoIntermedio.BuilderMepa;
import util.EscritorCodigoMepa;
import static util.Ref.*;

public class Traductor {

    private Token siguienteToken;
    private final CreadorTokens creador;
    private final AnalizadorLexicoPascal lexico;
    private final ManejadorDeTablas manejador;
    private final BuilderMepa mepa;

    public Traductor(String archivo) {
        lexico = new AnalizadorLexicoPascal(archivo);
        creador = new CreadorTokens();
        siguienteToken = lexico.obtenerToken();
        manejador = new ManejadorDeTablas();
        mepa = new BuilderMepa();
    }

    public void match(Token t) {
        if (siguienteToken.equals(t)) {
            try {
                siguienteToken = lexico.obtenerToken();
            } catch (Exception e) {
                errorSintactico("No pudo terminar correctamente y se esperaba "
                        + resaltar(t.toString()));
            }
        } else {
            errorSintactico("Se esperaba " + resaltar(t.getValor())
                    + " y se leyo " + resaltar(siguienteToken.getValor()));
        }
    }

    private boolean igualValor(String valor) {
        return siguienteToken.getValor().equals(valor);
    }

    private boolean igualTipo(String valor) {
        return siguienteToken.getTipo().equals(valor);
    }

    public void program() {
        String valorId;
        match(creador.getToken(PR_PROGRAM));
        //********************MEPA**********************//
        mepa.iniciarPrograma();
        //----------------------------------------------//
        valorId = id();
        match(creador.getToken(OP_PUNTO_Y_COMA));
        //********************MEPA**********************//
        String etiquetaProgram = mepa.generarEtiqueta();
        //----------------------------------------------//
        Procedure proc;
        proc = new Procedure(valorId, new ArrayList());
        manejador.crearTabla(proc, valorId);
        head();
        body();
        //********************MEPA**********************//
        mepa.liberarMemoria(manejador.obtenerCantidadVariables());
        mepa.finDePrograma();
        //----------------------------------------------//
        if (igualValor(OP_PUNTO)) {
            //********************MEPA**********************//
            EscritorCodigoMepa escritor = new EscritorCodigoMepa(lexico.obtenerDireccionAbsoluta());
            escritor.escribirFuente(mepa.getCodigo());
            //----------------------------------------------//
            System.out.println("Se termino la compilacion correctamente.");
        } else {
            errorSintactico("Se esperaba el punto final del programa y se leyo "
                    + resaltar(siguienteToken.getValor()));
        }
    }

    public void head() {
        String etiqueta;
        if (igualValor(PR_PROCEDURE) || igualValor(PR_FUNCTION)
                || igualValor(PR_VAR)) {
            switch (siguienteToken.getValor()) {
                case (PR_PROCEDURE):
                    //********************MEPA**********************//
                    etiqueta = mepa.generarEtiqueta();
                    mepa.saltarSiempre(etiqueta);
                    //----------------------------------------------//
                    procedure();
                    //********************MEPA**********************//
                    mepa.destinoDeSalto(etiqueta);
                    //----------------------------------------------//
                    break;
                case (PR_FUNCTION):
                    //********************MEPA**********************//
                    etiqueta = mepa.generarEtiqueta();
                    mepa.saltarSiempre(etiqueta);
                    //----------------------------------------------//
                    function();
                    //********************MEPA**********************//
                    mepa.destinoDeSalto(etiqueta);
                    //----------------------------------------------//
                    break;
                case (PR_VAR):
                    varDeclare();
                    break;
            }
            head();
        }
    }

    private void procedure() {
        ArrayList<Primitivo> listaParametro = new ArrayList<>();
        Procedure proc;
        String valorId;
        match(creador.getToken(PR_PROCEDURE));
        valorId = id();
        parameters(listaParametro);
        match(creador.getToken(OP_PUNTO_Y_COMA));
        //********************MEPA**********************//
        String etiqueta = mepa.generarEtiqueta();
        //----------------------------------------------//
        proc = new Procedure(valorId, listaParametro, etiqueta);
        manejador.insertar(proc);
        manejador.crearTabla(proc, valorId);
        //********************MEPA**********************//
        int anidamiento = manejador.obtenerAnidamiento();
        mepa.declararProcedimiento(etiqueta, anidamiento);
        //----------------------------------------------//
        manejador.insertarParametros(listaParametro);
        head();
        body();
        //********************MEPA**********************//
        //se libera la memoria
        int cantidadParametros = listaParametro.size();
        mepa.liberarMemoria(manejador.obtenerCantidadVariables() - cantidadParametros);
        mepa.finProcedimiento(anidamiento, cantidadParametros);
        //----------------------------------------------//
        match(creador.getToken(OP_PUNTO_Y_COMA));
        manejador.eliminarTabla();
    }

    private void function() {
        ArrayList<Primitivo> listaParametro = new ArrayList<>(), retorno = new ArrayList<>();
        ArrayList<String> valorId = new ArrayList<>();
        Function func;
        match(creador.getToken(PR_FUNCTION));
        valorId.add(id());
        parameters(listaParametro);
        match(creador.getToken(OP_DOS_PUNTOS));
        type(valorId, retorno);
        match(creador.getToken(OP_PUNTO_Y_COMA));
        Primitivo variableRetorno = retorno.get(0);
        //********************MEPA**********************//
        String etiqueta = mepa.generarEtiqueta();
        //----------------------------------------------//
        //retorno.get(0) es el tipo primitivo que retorna la funcion. El nombre
        //del primitivo es el mismo que el de la funcion
        func = new Function(valorId.get(0), listaParametro, variableRetorno);
        func.setEtiqueta(etiqueta);
        manejador.insertar(func);
        manejador.crearTabla(func, valorId.get(0));
        //********************MEPA**********************//
        int cantidadParametros = listaParametro.size();
        variableRetorno.setDesplazamiento(-cantidadParametros - 3);
        variableRetorno.setAnidamiento(manejador.obtenerAnidamiento());
        mepa.declararProcedimiento(etiqueta, manejador.obtenerAnidamiento());
        //----------------------------------------------//
        manejador.insertarParametros(listaParametro);
        head();
        body();
        //********************MEPA**********************//
        //se libera la memoria
        mepa.liberarMemoria(manejador.obtenerCantidadVariables() - cantidadParametros);
        mepa.finProcedimiento(manejador.obtenerAnidamiento(), cantidadParametros);
        //----------------------------------------------//
        match(creador.getToken(OP_PUNTO_Y_COMA));
        manejador.eliminarTabla();
    }

    private void parameters(ArrayList<Primitivo> listaParametro) {
        if (igualValor(OP_PARENTESIS_QUE_ABRE)) {
            match(creador.getToken(OP_PARENTESIS_QUE_ABRE));
            varType(listaParametro);
            moreParameters(listaParametro);
            match(creador.getToken(OP_PARENTESIS_QUE_CIERRA));
        }
    }

    private void moreParameters(ArrayList<Primitivo> listaParametro) {
        if (igualValor(OP_PUNTO_Y_COMA)) {
            match(creador.getToken(OP_PUNTO_Y_COMA));
            varType(listaParametro);
            moreParameters(listaParametro);
        }
    }

    private int varDeclare() {
        ArrayList<Primitivo> listaParametro = new ArrayList<>();
        match(creador.getToken(PR_VAR));
        varType(listaParametro);
        match(creador.getToken(OP_PUNTO_Y_COMA));
        moreVarType(listaParametro);
        manejador.insertar(listaParametro);
        //********************MEPA**********************//
        mepa.reservarMemoria(listaParametro.size());
        //----------------------------------------------//
        return listaParametro.size();
    }

    private void moreVarType(ArrayList<Primitivo> listaParametro) {
        if (igualTipo(IDENTIFICADOR)) {
            // si encuentra un id es que hay mas declaraciones
            varType(listaParametro);
            match(creador.getToken(OP_PUNTO_Y_COMA));
            moreVarType(listaParametro);
        }
    }

    private void varType(ArrayList<Primitivo> listaParametro) {
        ArrayList<String> listaId = new ArrayList<>();
        listaId.add(id());
        moreId(listaId);
        match(creador.getToken(OP_DOS_PUNTOS));
        type(listaId, listaParametro);
    }

    private void moreId(ArrayList<String> listaId) {
        if (igualValor(OP_COMA)) {
            match(creador.getToken(OP_COMA));
            listaId.add(id());
            moreId(listaId);
        } else if (igualTipo(IDENTIFICADOR)) {
            //hay 2 identificadores consecutivos por lo que falta la ,
            errorSintactico("Falta una " + resaltar(OP_COMA)
                    + " entre los identificadores");
        }
    }

    private void type(ArrayList<String> listaId, ArrayList<Primitivo> listaParametro) {
        switch (siguienteToken.getValor()) {
            case (PR_BOOLEAN):
                match(creador.getToken(PR_BOOLEAN));
                for (String id : listaId) {
                    listaParametro.add(new Booleano(id));
                }
                break;
            case (PR_INTEGER):
                match(creador.getToken(PR_INTEGER));
                for (String id : listaId) {
                    listaParametro.add(new Entero(id));
                }
                break;
            default:
                errorSemantico("Se esperaba el tipo " + resaltar(PR_BOOLEAN) + " o " + resaltar(PR_INTEGER)
                        + " de las variables y se leyo " + resaltar(siguienteToken.getValor()));
        }
    }

    public void body() {
        match(creador.getToken(PR_BEGIN));
        sentence();
        match(creador.getToken(PR_END));

    }

    private void sentence() {
        if (igualValor(PR_IF) || igualValor(PR_WHILE) || igualValor(PR_BEGIN)
                || igualTipo(IDENTIFICADOR) || igualValor(PR_WRITE)
                || igualValor(PR_READ)) {
            sentenceAux();
            sentenceAux2();
        }
    }

    private void sentenceAux() {
        Identificador id;
        String valorId;
        if (igualTipo(IDENTIFICADOR)) {
            valorId = id();
            id = manejador.chequearExistencia(valorId);
            callOrAssig(id);
        } else {
            switch (siguienteToken.getValor()) {
                case (PR_IF):
                    ifControl();
                    break;
                case (PR_WHILE):
                    whileControl();
                    break;
                case (PR_WRITE):
                    writeControl();
                    break;
                case (PR_READ):
                    readControl();
                    break;
                case (PR_BEGIN):
                    body();
                    break;
                default:
                    errorSintactico("Se esperaba una llamada a procedimiento/funcion "
                            + "o una instruccion if, while, de asignacion o el begin de "
                            + "un bloque y se leyo " + resaltar(siguienteToken.getValor()));
            }
        }
    }

    private void sentenceAux2() {
        if (igualValor(OP_PUNTO_Y_COMA)) {
            match(creador.getToken(OP_PUNTO_Y_COMA));
            sentence();
        }
    }

    private void writeControl() {
        match(creador.getToken(PR_WRITE));
        match(creador.getToken(OP_PARENTESIS_QUE_ABRE));
        //chequeo si la expresion es del tipo entero
        if (exp().esEntero()) {
            match(creador.getToken(OP_PARENTESIS_QUE_CIERRA));
            //********************MEPA**********************//
            mepa.imprimir();
            //----------------------------------------------//
        } else {
            errorSemantico("Se esperaba que la expresión que reciba WRITE sea "
                    + "del tipo entero ");
        }
    }

    //Hay que agregarle validador semantico que el id sea un primitivo
    private void readControl() {
        Identificador id;
        match(creador.getToken(PR_READ));
        match(creador.getToken(OP_PARENTESIS_QUE_ABRE));
        id = manejador.chequearExistencia(id());

        //si el id es del tipo entero
        if (id.getClass() == Entero.class) {
            match(creador.getToken(OP_PARENTESIS_QUE_CIERRA));
            //********************MEPA**********************//
            mepa.leer((Entero) id);
            //----------------------------------------------//
        } else {
            errorSemantico("Se esperaba que el identificador" + resaltar(id.getNombre())
                    + " que recibe el read sea del tipo entero ");
        }
    }

    private void callOrAssig(Identificador id) {
        ArrayList<Primitivo> listaParametro = new ArrayList<>();
        switch (siguienteToken.getValor()) {
            case (OP_ASIGNACION):
                if (id.getClass() != Procedure.class) {
                    assig(id);
                } else {
                    errorSintactico("El procedimiento '" + id.getNombre()
                            + "' no puede estar del lado izquierdo de una asignacion");
                }
                break;
            default: //callParameter puede ser lambda por lo que no se sabe
                //que puede venir después
                if (id.getClass() == Function.class) {
                    //reservo 1 para el retorno
                    //********************MEPA**********************//
                    mepa.reservarMemoria(1);
                    //----------------------------------------------//
                }
                callParameter(listaParametro);
                //el id se encarga de retornar el error correspondiente si no lo son
                id.parametrosCorrectos(listaParametro);
                //********************MEPA**********************//
                mepa.llamarProcedimiento(((Procedure) id).getEtiqueta());
            //----------------------------------------------//
        }
    }

    private void callParameter(ArrayList<Primitivo> listaParametro) {
        if (igualValor(OP_PARENTESIS_QUE_ABRE)) {
            match(creador.getToken(OP_PARENTESIS_QUE_ABRE));
            listaParametro.add(exp());
            moreExpParameter(listaParametro);
            match(creador.getToken(OP_PARENTESIS_QUE_CIERRA));
        }
    }

    private void moreExpParameter(ArrayList<Primitivo> listaParametro) {
        if (igualValor(OP_COMA)) {
            match(creador.getToken(OP_COMA));
            listaParametro.add(exp());
            moreExpParameter(listaParametro);
        }
    }

    public String id() {
        String valorId = "";
        if (igualTipo(IDENTIFICADOR)) {
            valorId = siguienteToken.getValor();
            siguienteToken = lexico.obtenerToken();
        } else if (igualTipo(PALABRA_RESERVADA)) {
            errorSintactico("La palabra reservada " + resaltar(siguienteToken.getValor()) + " no puede ser un identificador");
        } else if (igualTipo(OPERADOR_DE_ASIGNACION)) {
            errorSintactico("Falta el identificador de una variable antes del operador "
                    + resaltar(siguienteToken.getValor()));
        } else if (igualTipo(OPERADOR_DE_PUNTUACION)) {
            errorSintactico("Falta un " + resaltar("identificador")
                    + " antes del operador " + resaltar(siguienteToken.getValor()));
        } else {
            errorSintactico("Identificador no valido: " + resaltar(siguienteToken.getValor()));
        }
        return valorId;
    }

    private void assig(Identificador id) {
        if (id.getClass() == Function.class
                && !manejador.esNombreTable(id.getNombre())) {
            errorSemantico("Se intenta asignar un valor a la funcion " + resaltar(id.getNombre()) + " fuera de su ambiente. "
                    + "Solo se le puede asignar valor dentro de su ambiente");
        } else {
            match(creador.getToken(OP_ASIGNACION));
            Primitivo tipo = exp();
            Primitivo variable;
            if (id.getClass() == Function.class) {
                variable = ((Function) id).getRetorno();
            } else {
                variable = (Primitivo) id;
            }
            //********************MEPA**********************//
            mepa.asignarVariable(variable);
            //----------------------------------------------//
            if (!id.esMismoTipo(tipo)) {
                errorSemantico("El identificador " + resaltar(id.getNombre())
                        + " es del tipo " + resaltar(id.toStringTipo()) + ""
                        + " y se le quiere asignar un " + resaltar(tipo.toStringTipo()));
            }
        }
    }

    private void ifControl() {
        match(creador.getToken(PR_IF));
        //if (exp)
        if (exp().esBooleano()) {
            //then
            match(creador.getToken(PR_THEN));
            //********************MEPA**********************//
            String etiqueta = mepa.generarEtiqueta();
            mepa.saltarSiFalso(etiqueta);
            //----------------------------------------------//
            sentenceAux();

            ifElse(etiqueta);
        } else {
            errorSemantico("La condicion del " + resaltar(PR_IF)
                    + " no es del tipo " + resaltar(PR_BOOLEAN));
        }
    }

    private void ifElse(String etiqueta) {
        if (igualValor(PR_ELSE)) {
            //********************MEPA**********************//
            String etiquetaElse = mepa.generarEtiqueta();
            mepa.saltarSiempre(etiquetaElse);
            //----------------------------------------------//
            match(creador.getToken(PR_ELSE));
            //********************MEPA**********************//
            mepa.destinoDeSalto(etiqueta);
            //----------------------------------------------//
            sentenceAux();
            //********************MEPA**********************//
            mepa.destinoDeSalto(etiquetaElse);
            //----------------------------------------------//
        } else {
            //********************MEPA**********************//
            mepa.destinoDeSalto(etiqueta);
            //----------------------------------------------//
        }

    }

    private void whileControl() {
        match(creador.getToken(PR_WHILE));
        //********************MEPA**********************//
        String etiquetaPrincipioWhile = mepa.generarEtiqueta();
        mepa.destinoDeSalto(etiquetaPrincipioWhile);
        //----------------------------------------------//
        if (exp().esBooleano()) {
            //********************MEPA**********************//
            String etiquietaFueraDelWhile = mepa.generarEtiqueta();
            mepa.saltarSiFalso(etiquietaFueraDelWhile);
            //----------------------------------------------//
            match(creador.getToken(PR_DO));
            sentenceAux();
            //********************MEPA**********************//
            mepa.saltarSiempre(etiquetaPrincipioWhile);
            mepa.destinoDeSalto(etiquietaFueraDelWhile);
            //----------------------------------------------//
        } else {
            errorSemantico("La condicion del " + resaltar(PR_WHILE)
                    + " no es del tipo " + resaltar(PR_BOOLEAN));
        }
    }

    private Primitivo value() {
        Primitivo tipo;
        if (igualValor(PR_FALSE) || igualValor(PR_TRUE)) {
            booleanControl();
            tipo = new Booleano("");
        } else {
            number();
            tipo = new Entero("");
        }
        return tipo;
    }

    private void number() {
        if (igualTipo(NUMERO)) {
            //********************MEPA**********************//
            mepa.apilarContante(Integer.parseInt(siguienteToken.getValor()));
            //----------------------------------------------//
            siguienteToken = lexico.obtenerToken();
        } else {
            errorSintactico("Se esperaba un " + resaltar(NUMERO)
                    + " y se leyo " + resaltar(siguienteToken.getValor()));
        }
    }

    private Primitivo exp() {
        Primitivo tipo;
        tipo = exp1();
        expAux(tipo);
        return tipo;

    }

    private void expAux(Primitivo tipo) {
        if (igualValor(PR_OR)) {
            match(creador.getToken(PR_OR));
            //Chequea si el tipo que recibio y el que retorna exp1 es booleano
            if (tipo.esBooleano() && exp1().esBooleano()) {
                //********************MEPA**********************//
                mepa.operar(PR_OR);
                //----------------------------------------------//
                expAux(tipo);
            } else {
                errorSemantico("El operador " + resaltar(PR_OR)
                        + " funciona solo con " + resaltar(PR_BOOLEAN));
            }

        }
    }

    private Primitivo exp1() {
        Primitivo tipo;
        tipo = exp2();
        expAux1(tipo);
        return tipo;
    }

    private void expAux1(Primitivo tipo) {
        if (igualValor(PR_AND)) {
            match(creador.getToken(PR_AND));
            if (tipo.esBooleano() && exp2().esBooleano()) {
                //********************MEPA**********************//
                mepa.operar(PR_AND);
                //----------------------------------------------//
                expAux1(tipo);
            } else {
                errorSemantico("El operador " + resaltar(PR_AND)
                        + " funciona solo con " + resaltar(PR_BOOLEAN));
            }
        }
    }

    private Primitivo exp2() {
        Primitivo tipo;
        tipo = exp3();
        tipo = expAux2(tipo);
        return tipo;
    }

    private Primitivo expAux2(Primitivo tipo) {
        String operador;
        if (igualValor(OP_IGUAL) || igualValor(OP_DISTINTO) || igualValor(OP_MAYOR)
                || igualValor(OP_MENOR) || igualValor(OP_MAYOR_IGUAL) || igualValor(OP_MENOR_IGUAL)) {
            operador = op3(tipo);
            if (tipo.esMismoTipo(exp3())) {
                tipo = new Booleano("");
                //********************MEPA**********************//
                mepa.operar(operador);
                //----------------------------------------------//
                expAux2(tipo);

            } else {
                errorSemantico("El operador " + resaltar(operador)
                        + " funciona solo con expresiones del " + resaltar("mismo tipo"));
            }
        }
        return tipo;
    }

    private Primitivo exp3() {
        Primitivo tipo;
        tipo = exp4();
        expAux3(tipo);
        return tipo;
    }

    private void expAux3(Primitivo tipo) {
        String operador;
        if (igualValor(OP_SUMA) || igualValor(OP_RESTA)) {
            operador = op2();
            if (tipo.esEntero() && exp4().esEntero()) {
                //********************MEPA**********************//
                mepa.operar(operador);
                //----------------------------------------------//
                expAux3(tipo);
            } else {
                errorSemantico("El operador " + resaltar(operador)
                        + " funciona solo con " + resaltar("entero"));
            }
        }
    }

    private Primitivo exp4() {
        Primitivo tipo;
        tipo = exp5();
        expAux4(tipo);
        return tipo;
    }

    private void expAux4(Primitivo tipo) {
        String operador;
        if (igualValor(OP_MULTIPLICACION) || igualValor(OP_DIVISION)) {
            operador = op1();
            if (tipo.esEntero() && exp5().esEntero()) {
                //********************MEPA**********************//
                mepa.operar(operador);
                //----------------------------------------------//
                expAux4(tipo);
            } else {
                errorSemantico("El operador " + resaltar(operador)
                        + " funciona solo con " + resaltar("entero"));
            }

        }
    }

    private Primitivo exp5() {
        Primitivo tipo = null;
        if (igualValor(PR_NOT)) {
            match(creador.getToken(PR_NOT));
            if (exp5().esBooleano()) {
                tipo = new Booleano("");
                //********************MEPA**********************//
                mepa.operar(PR_NOT);
                //----------------------------------------------//
            } else {
                errorSemantico("El operador " + resaltar(PR_NOT)
                        + " funciona solo con " + resaltar(PR_BOOLEAN));
            }
        } else {
            tipo = exp6();
        }
        return tipo;
    }

    private Primitivo exp6() {
        Primitivo tipo = null;
        if (igualValor(OP_RESTA)) {
            match(creador.getToken(OP_RESTA));
            if (exp6().esEntero()) {
                tipo = new Entero("");
                //********************MEPA**********************//
                mepa.operarRestaUnaria();
                //----------------------------------------------//
            } else {
                errorSemantico("El operador " + resaltar(OP_RESTA)
                        + " funciona solo con " + resaltar("entero"));
            }
        } else {
            tipo = fact();
        }
        return tipo;
    }

    private Primitivo fact() {
        Primitivo tipo = null;
        if (igualTipo(NUMERO) || igualValor(PR_FALSE) || igualValor(PR_TRUE)) {
            tipo = value();
        } else if (igualTipo(IDENTIFICADOR)) {
            String valorId = id();
            ArrayList<Primitivo> listaParametros = new ArrayList<>();
            Identificador id = manejador.chequearExistencia(valorId);
            if (id.getClass() != Procedure.class) {

                if (id.getClass() == Function.class || !listaParametros.isEmpty()) {
                    //********************MEPA**********************//
                    mepa.reservarMemoria(1);
                    //----------------------------------------------//
                    callParameter(listaParametros);
                    id.parametrosCorrectos(listaParametros);
                    Function func = (Function) id;
                    //********************MEPA**********************//
                    mepa.llamarProcedimiento(func.getEtiqueta());
                    //----------------------------------------------//
                }

                if (id.getClass() == Booleano.class || id.getClass() == Entero.class) {
                    //********************MEPA**********************//
                    mepa.apilarVariable((Primitivo) id);
                    //----------------------------------------------//
                }

                tipo = id.getTipo();

            } else {
                errorSemantico("Un procedimiento no puede estar en el lado de "
                        + "derecho de una expresion ni como condicion");

            }
        } else if (igualValor(OP_PARENTESIS_QUE_ABRE)) {
            match(creador.getToken(OP_PARENTESIS_QUE_ABRE));
            tipo = exp();
            match(creador.getToken(OP_PARENTESIS_QUE_CIERRA));
        } else {
            errorSintactico("Se esperaba una expresion y se leyo " + resaltar(siguienteToken.getValor()));
        }
        return tipo;
    }

    private String op3(Primitivo tipo) {
        String operador = "";
        if (igualTipo(OPERADOR_LOGICO)) {
            operador = siguienteToken.getValor();
            switch (operador) {
                case (OP_IGUAL):
                    match(creador.getToken(OP_IGUAL));
                    break;
                case (OP_DISTINTO):
                    match(creador.getToken(OP_DISTINTO));
                    break;
                case (OP_MAYOR):
                    match(creador.getToken(OP_MAYOR));
                    if (!tipo.esEntero()) {
                        errorSemantico("El operador " + resaltar(operador) + " funciona solo con"
                                + " " + resaltar("entero"));
                    }
                    break;
                case (OP_MENOR):
                    match(creador.getToken(OP_MENOR));
                    if (!tipo.esEntero()) {
                        errorSemantico("El operador " + resaltar(operador) + " funciona solo con "
                                + resaltar("entero"));
                    }
                    break;
                case (OP_MAYOR_IGUAL):
                    match(creador.getToken(OP_MAYOR_IGUAL));
                    if (!tipo.esEntero()) {
                        errorSemantico("El operador " + resaltar(operador) + " funciona solo con "
                                + resaltar("entero"));
                    }
                    break;
                case (OP_MENOR_IGUAL):
                    match(creador.getToken(OP_MENOR_IGUAL));
                    if (!tipo.esEntero()) {
                        errorSemantico("El operador " + resaltar(operador) + " funciona solo con "
                                + "" + resaltar("entero"));
                    }
                    break;
                default:
                    //operador logico distinto a los definidos???
                    errorSintactico("El " + OPERADOR_LOGICO.toLowerCase() + " "
                            + resaltar(siguienteToken.getValor())
                            + " no es correcto");
            }
        } else {
            errorSintactico("Se esperaba un " + OPERADOR_LOGICO.toLowerCase()
                    + " y se leyo " + resaltar(siguienteToken.getValor()));
        }
        return operador;
    }

    private String op2() {
        String operador = siguienteToken.getValor();
        switch (operador) {
            case (OP_SUMA):
                match(creador.getToken(OP_SUMA));
                break;
            case (OP_RESTA):
                match(creador.getToken(OP_RESTA));
                break;
            default:
                errorSintactico("Se esperaba un operador de adicion o sustraccion "
                        + "y se leyo " + resaltar(siguienteToken.getValor()));
        }
        return operador;
    }

    private String op1() {
        String operador = siguienteToken.getValor();
        switch (operador) {
            case (OP_MULTIPLICACION):
                match(creador.getToken(OP_MULTIPLICACION));
                break;
            case (OP_DIVISION):
                match(creador.getToken(OP_DIVISION));
                break;
            default:
                errorSintactico("Se esperaba un operador de multiplicacion o division "
                        + "y se leyo " + resaltar(siguienteToken.getValor()));
        }
        return operador;
    }

    private void booleanControl() {
        switch (siguienteToken.getValor()) {
            case (PR_TRUE):
                match(creador.getToken(PR_TRUE));
                //********************MEPA**********************//
                mepa.apilarContante(1);
                //----------------------------------------------//
                break;
            case (PR_FALSE):
                match(creador.getToken(PR_FALSE));
                //********************MEPA**********************//
                mepa.apilarContante(0);
                //----------------------------------------------//
                break;
            default:
                errorSintactico("Se espera un elemento booleano y se leyo "
                        + resaltar(siguienteToken.getValor()));
        }
    }
}
