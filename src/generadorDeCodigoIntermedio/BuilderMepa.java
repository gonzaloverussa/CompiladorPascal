package generadorDeCodigoIntermedio;

import analisadorSemantico.Entero;
import analisadorSemantico.Primitivo;
import static util.Ref.*;

public class BuilderMepa {

    private String codigoMepa;
    private int contadorEtiqueta;

    public BuilderMepa() {
        codigoMepa = "";
        contadorEtiqueta = 1;
    }

    public void reservarMemoria(int cant) {
        codigoMepa += "RMEM " + cant + " \n";
    }

    public void liberarMemoria(int cant) {
        codigoMepa += "LMEM " + cant + " \n";
    }

    public void iniciarPrograma() {
        codigoMepa += "INPP \n";
    }

    public void operar(String operador) {
        switch (operador) {
            case (OP_IGUAL):
                codigoMepa += "CMIG";
                break;
            case (OP_DISTINTO):
                codigoMepa += "CMDG";
                break;
            case (OP_MAYOR):
                codigoMepa += "CMMA";
                break;
            case (OP_MENOR):
                codigoMepa += "CMME";
                break;
            case (OP_MAYOR_IGUAL):
                codigoMepa += "CMYI";
                break;
            case (OP_MENOR_IGUAL):
                codigoMepa += "CMNI";
                break;
            case (OP_SUMA):
                codigoMepa += "SUMA";
                break;
            case (OP_RESTA):
                codigoMepa += "SUST";
                break;
            case (OP_MULTIPLICACION):
                codigoMepa += "MULT";
                break;
            case (OP_DIVISION):
                codigoMepa += "DIVI";
                break;
            case (PR_NOT):
                codigoMepa += "NEGA";
                break;
            case (PR_AND):
                codigoMepa += "CONJ";
                break;
            case (PR_OR):
                codigoMepa += "DISJ";
                break;
        }
        codigoMepa += "\n";
    }

    public void operarRestaUnaria() {
        codigoMepa += "UMEN \n";
    }

    public void apilarVariable(Primitivo var) {
        codigoMepa += "APVL " + var.getAnidamiento() + " " + var.getDesplazamiento() + " \n";
    }

    public void asignarVariable(Primitivo var) {
        codigoMepa += "ALVL " + var.getAnidamiento() + " " + var.getDesplazamiento() + " \n";
    }

    public void imprimir() {
        codigoMepa += "IMPR \n";
    }

    public void leer(Entero var) {
        codigoMepa += "LEER \n";
        asignarVariable(var);
    }

    public String generarEtiqueta() {
        String e = "l" + contadorEtiqueta;
        contadorEtiqueta++;
        return e;
    }

    public void apilarContante(int i) {
        codigoMepa += "APCT " + i + " \n";
    }

    public void saltarSiFalso(String etiqueta) {
        codigoMepa += "DSVF " + etiqueta + " \n";
    }

    public void destinoDeSalto(String etiqueta) {
        codigoMepa += etiqueta + " NADA \n";

    }

    public void saltarSiempre(String etiqueta) {
        codigoMepa += "DSVS " + etiqueta + " \n";
    }

    public String getCodigo() {
        return codigoMepa;
    }

    public void finDePrograma() {
        codigoMepa += "PARA \n";
    }

    public void declararProcedimiento(String etiqueta, int anidamiento) {
        codigoMepa += etiqueta + " ENPR " + anidamiento + " \n";
    }

    public void llamarProcedimiento(String etiqueta) {
        codigoMepa += "LLPR " + etiqueta + " \n";
    }

    public void finProcedimiento(int anidamiento, int cantParametros) {
        codigoMepa += "RTPR " + anidamiento + " " + cantParametros + " \n";
    }
}
