package compiladores;

import analisadorSintactico.Traductor;

public class main {

    public static void main(String[] args) {
        if (args.length == 1) {
            Traductor sintax = new Traductor(args[0]);
            sintax.program();
        } else {
            System.err.println("Se debe poner el nombre del archivo como parametro");
        }
    }
}
