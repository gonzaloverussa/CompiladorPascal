package analisadorLexico;

import java.util.HashMap;
import static util.Ref.*;

public class CreadorTokens {

    private final HashMap<String, Token> mapa;

    public CreadorTokens() {
        mapa = new HashMap<>();
        mapa.put(OP_MENOR, new Token(OPERADOR_LOGICO, OP_MENOR));
        mapa.put(OP_MENOR_IGUAL, new Token(OPERADOR_LOGICO, OP_MENOR_IGUAL));
        mapa.put(OP_MAYOR, new Token(OPERADOR_LOGICO, OP_MAYOR));
        mapa.put(OP_MAYOR_IGUAL, new Token(OPERADOR_LOGICO, OP_MAYOR_IGUAL));
        mapa.put(OP_DISTINTO, new Token(OPERADOR_LOGICO, OP_DISTINTO));
        mapa.put(OP_IGUAL, new Token(OPERADOR_LOGICO, OP_IGUAL));
        mapa.put(OP_SUMA, new Token(OPERADOR_ARITMETICO, OP_SUMA));
        mapa.put(OP_RESTA, new Token(OPERADOR_ARITMETICO, OP_RESTA));
        mapa.put(OP_MULTIPLICACION, new Token(OPERADOR_ARITMETICO, OP_MULTIPLICACION));
        mapa.put(OP_DIVISION, new Token(OPERADOR_ARITMETICO, OP_DIVISION));
        mapa.put(OP_ASIGNACION, new Token(OPERADOR_DE_ASIGNACION, OP_ASIGNACION));
        mapa.put(OP_PUNTO, new Token(OPERADOR_DE_PUNTUACION, OP_PUNTO));
        mapa.put(OP_COMA, new Token(OPERADOR_DE_PUNTUACION, OP_COMA));
        mapa.put(OP_PUNTO_Y_COMA, new Token(OPERADOR_DE_PUNTUACION, OP_PUNTO_Y_COMA));
        mapa.put(OP_DOS_PUNTOS, new Token(OPERADOR_DE_PUNTUACION, OP_DOS_PUNTOS));
        mapa.put(OP_PARENTESIS_QUE_ABRE, new Token(OPERADOR_DE_PUNTUACION, OP_PARENTESIS_QUE_ABRE));
        mapa.put(OP_PARENTESIS_QUE_CIERRA, new Token(OPERADOR_DE_PUNTUACION, OP_PARENTESIS_QUE_CIERRA));
        mapa.put(PR_PROGRAM, new Token(PALABRA_RESERVADA, PR_PROGRAM));
        mapa.put(PR_PROCEDURE, new Token(PALABRA_RESERVADA, PR_PROCEDURE));
        mapa.put(PR_FUNCTION, new Token(PALABRA_RESERVADA, PR_FUNCTION));
        mapa.put(PR_BOOLEAN, new Token(PALABRA_RESERVADA, PR_BOOLEAN));
        mapa.put(PR_INTEGER, new Token(PALABRA_RESERVADA, PR_INTEGER));
        mapa.put(PR_VAR, new Token(PALABRA_RESERVADA, PR_VAR));
        mapa.put(PR_BEGIN, new Token(PALABRA_RESERVADA, PR_BEGIN));
        mapa.put(PR_END, new Token(PALABRA_RESERVADA, PR_END));
        mapa.put(PR_IF, new Token(PALABRA_RESERVADA, PR_IF));
        mapa.put(PR_THEN, new Token(PALABRA_RESERVADA, PR_THEN));
        mapa.put(PR_ELSE, new Token(PALABRA_RESERVADA, PR_ELSE));
        mapa.put(PR_WHILE, new Token(PALABRA_RESERVADA, PR_WHILE));
        mapa.put(PR_DO, new Token(PALABRA_RESERVADA, PR_DO));
        mapa.put(PR_AND, new Token(PALABRA_RESERVADA, PR_AND));
        mapa.put(PR_OR, new Token(PALABRA_RESERVADA, PR_OR));
        mapa.put(PR_TRUE, new Token(PALABRA_RESERVADA, PR_TRUE));
        mapa.put(PR_FALSE, new Token(PALABRA_RESERVADA, PR_FALSE));
        mapa.put(PR_NOT, new Token(PALABRA_RESERVADA, PR_NOT));
        mapa.put(PR_WRITE, new Token(PALABRA_RESERVADA, PR_WRITE));
        mapa.put(PR_READ, new Token(PALABRA_RESERVADA, PR_READ));
    }

    public Token getToken(String valor) {
        Token token;
        token = mapa.get(valor.toLowerCase());
        if (token == null) {
            //El operador no existe
            token = new Token(OPERADOR_NO_VALIDO, valor);
        }
        return token;
    }

    public Token getToken(String tipo, String valor) {
        //solo de debe mandar id y numeros
        Token token;
        if (tipo.equals(IDENTIFICADOR)) {
            token = mapa.get(valor.toLowerCase());
            if (token == null) {
                token = new Token(IDENTIFICADOR, valor.toLowerCase());
            }
        } else {
            token = new Token(NUMERO, valor);
        }
        return token;
    }
}
