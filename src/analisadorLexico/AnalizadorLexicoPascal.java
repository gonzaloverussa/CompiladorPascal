package analisadorLexico;

import util.LectorArchivoFuente;
import java.io.FileNotFoundException;
import java.util.LinkedList;
import static util.Ref.*;
import static errores.Error.errorLexico;
import static errores.Error.warning;
import static errores.Error.resaltar;

public class AnalizadorLexicoPascal {

    private LectorArchivoFuente lector;
    private static String cadena;
    private LinkedList<Token> listaTokens;
    private static int linea;

    public static CreadorTokens creador;

    public AnalizadorLexicoPascal(String archivo) {
        listaTokens = new LinkedList<>();
        creador = new CreadorTokens();
        linea = 0;
        try {
            lector = new LectorArchivoFuente(archivo);
        } catch (FileNotFoundException e) {
            errorLexico("No se pudo abrir el archivo. Revise el nombre del"
                    + " archivo, este debe incluir la extension del mismo");
        }
    }

    public String obtenerDireccionAbsoluta() {
        return lector.obtenerDireccionAbsoluta();
    }

    public Token obtenerToken() {
        if (listaTokens.isEmpty()) {
            obtenerLineaConToken();
            /*for (Token t: listaTokens) {  //imprime todos los tokens generados
		System.out.println(t.toString());
	    }*/
        } else {
            linea = lector.numeroLinea();
        }
        return listaTokens.pop();
    }

    public static int obtenerNroLinea() {
        return linea;
    }

    private void automata() {
        //La cadena analizar y por donde comienza el analisis
        char puntero;
        String valorToken;
        int longitud = cadena.length();
        if (!cadena.equals("")) {
            puntero = cadena.charAt(0);
            //Verifico si la primer palabra es letra, considerando letra
            //como lo definido por nuestro alfabeto
            if (esLetra(puntero)) {
                palabraReservadaOId();
                automata();
                //Verifico si la primer letra es un digito
            } else if (Character.isDigit(puntero)) {
                numero();
                automata();
            } else {
                //la mayoria de los casos se repiten lo que se debe hacer por lo que se
                switch (puntero) {
                    //espacio en blanco
                    case ' ':
                    //Tabs
                    case '	':
                    //salto de linea	
                    case '\n':
                        //se consume el caracter y sigue
                        cadena = cadena.substring(1);
                        automata();
                        break;
                    //inicio de comentario
                    case '{':
                        //se consume todo lo que halla a continuacion hasta
                        //encontrar el cierre de comentario
                        consumirComentario();
                        automata();
                        break;
                    //token : o :=
                    case ':':
                    //token > o >=
                    case '>':
                        //se verifica que halla al menos un caracter mas en la
                        //cadena y si lo hay que sea un igual
                        if (longitud > 1 && cadena.charAt(1) == '=') {
                            //token := o >=
                            valorToken = cadena.substring(0, 2);
                            cadena = cadena.substring(2);
                            listaTokens.add(creador.getToken(valorToken));
                            automata();
                        } else {
                            //: o >
                            valorToken = cadena.substring(0, 1);
                            cadena = cadena.substring(1);
                            listaTokens.add(creador.getToken(valorToken));
                            automata();
                        }
                        break;
                    case '<':
                        //< <= o <>
                        if (longitud > 1 && (cadena.charAt(1) == '='
                                || cadena.charAt(1) == '>')) {
                            //token <= o <>
                            switch (cadena.charAt(1)) {
                                //token <=
                                case '=':
                                //token <>
                                case '>':
                                    valorToken = cadena.substring(0, 2);
                                    cadena = cadena.substring(2);
                                    listaTokens.add(creador.getToken(valorToken));
                                    automata();
                                    break;
                            }
                        } else {
                            //token <
                            valorToken = cadena.substring(0, 1);
                            cadena = cadena.substring(1);
                            listaTokens.add(creador.getToken(valorToken));
                            automata();
                        }
                        break;
                    //token_suma	
                    case '+':
                    //token_Resta	
                    case '-':
                    //Token_multiplicacion	
                    case '*':
                    //Token_Division	
                    case '/':
                    //Token_parentesis que abre	
                    case '(':
                    //token_parentesis_que_cierra	
                    case ')':
                    //token igual	
                    case '=':
                    //token_coma	
                    case ',':
                    //token_fin_sentencia	
                    case ';':
                    //token_fin_programa	
                    case '.':
                        //se consume un caracter, se crea el token conrrespondiente y se sigue
                        valorToken = cadena.substring(0, 1);
                        cadena = cadena.substring(1);
                        listaTokens.add(creador.getToken(valorToken));
                        automata();
                        break;

                    //caracter de cierre de comentario	
                    case '}':
                        //se encontro un cierre de comentario sin un caracter de apertura de comentario previo
                        //se consume el caracter, se miestra warning y se continua como si no estuviera
                        valorToken = cadena.substring(0, 1);
                        cadena = cadena.substring(1);
                        warning(resaltar(valorToken) + " para cerrar un comentario"
                                + " que no fue previamente abierto. Sera ignorado");
                        automata();
                        break;
                    //caracter fuera del alfabeto
                    default:
                        //se consume el caracter, se miestra warning y se continua como si no estuviera
                        valorToken = cadena.substring(0, 1);
                        cadena = cadena.substring(1);
                        warning(resaltar(valorToken) + " que no es valido. Sera ignorado");
                        automata();
                        break;
                }
            }
        }
    }

    private void consumirComentario() {
        //Consume la cadena hasta encontrar el fin del comentario o el fin del archivo
        int posicion;
        while ((cadena.indexOf("}")) == -1) {
            cadena = lector.retornarLinea();
            if (cadena.equalsIgnoreCase("Fin de archivo")) {
                //Se llegó al fin de archivo
                errorLexico("Se termino el codigo porque quedo un comentario sin cerrar");
            }
        }
        //Se encontró el fin de comentario
        posicion = cadena.indexOf("}");
        //Se avanza hasta el siguiente caracter al fin de comentario
        cadena = cadena.substring(posicion + 1);
    }

    private void numero() {
        //Si el futuro token empieza con numero, entonces es un numeor
        int longitud, i = 1;
        char caracter;
        longitud = cadena.length();
        String cadenaConsumida = String.valueOf(cadena.charAt(0));
        boolean tokenEncontrado = false;
        while (!tokenEncontrado && i < longitud) {
            caracter = cadena.charAt(i);
            if (Character.isDigit(caracter)) {
                cadenaConsumida = cadenaConsumida + caracter;
                i++;
            } else {
                tokenEncontrado = true;
            }
        }
        listaTokens.add(creador.getToken(NUMERO, cadenaConsumida));
        cadena = cadena.substring(i);
    }

    private void palabraReservadaOId() {
        //Si se encontro una letra se crea un token de id o de palabra reservada
        int longitud, i = 1;
        char caracter;
        longitud = cadena.length();
        String cadenaConsumida = String.valueOf(cadena.charAt(0));
        boolean terminoToken = false;
        while (!terminoToken && i < longitud) {
            caracter = cadena.charAt(i);
            if (esLetra(caracter) || Character.isDigit(caracter)) {
                cadenaConsumida = cadenaConsumida + caracter;
                i++;
            } else {
                terminoToken = true;
            }
        }
        //Se crea un token en la lista, el objeto CreadorToken se encarga de saber si es una palabra reservada o un id
        listaTokens.add(creador.getToken(IDENTIFICADOR, cadenaConsumida));
        cadena = cadena.substring(i);
    }

    private static boolean esLetra(char puntero) {
        boolean respuesta = false;
        //Verifico si es una letra segun definido en nuestro alfabeto comparando con los valores de la tabla ascii
        if ((puntero >= 65 && puntero <= 90) || (puntero >= 97 && puntero <= 122) || puntero == 95) {
            respuesta = true;
        }
        return respuesta;
    }

    private void obtenerLineaConToken() {
        cadena = lector.retornarLinea();
        if (cadena.equalsIgnoreCase("fin de archivo")) {
            //finalizo la lectura del archivo y todavia no se termino de compilar
            errorLexico("Se termino el codigo y falta cerrar el programa");
        } else {
            //crea los tokens correspondientes de la linea
            automata();
        }
        if (listaTokens.isEmpty()) {
            obtenerLineaConToken();
        }
    }
}
