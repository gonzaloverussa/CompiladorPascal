package util;

public class Ref {

    public final static String PALABRA_RESERVADA = "Palabra Reservada";
    public final static String OPERADOR_LOGICO = "Operador Logico";
    public final static String OPERADOR_ARITMETICO = "Operador Aritmetico";
    public final static String OPERADOR_DE_ASIGNACION = "Operador de Asignacion";
    public final static String OPERADOR_DE_PUNTUACION = "Caracter de Puntuacion";
    public final static String OPERADOR_NO_VALIDO = "Caracter No valido";
    public final static String IDENTIFICADOR = "Id";
    public final static String NUMERO = "Numero";

    public final static String PR_PROGRAM = "program";
    public final static String PR_PROCEDURE = "procedure";
    public final static String PR_FUNCTION = "function";
    public final static String PR_BOOLEAN = "boolean";
    public final static String PR_INTEGER = "integer";
    public final static String PR_VAR = "var";
    public final static String PR_BEGIN = "begin";
    public final static String PR_END = "end";
    public final static String PR_IF = "if";
    public final static String PR_THEN = "then";
    public final static String PR_ELSE = "else";
    public final static String PR_WHILE = "while";
    public final static String PR_DO = "do";
    public final static String PR_AND = "and";
    public final static String PR_OR = "or";
    public final static String PR_TRUE = "true";
    public final static String PR_FALSE = "false";
    public final static String PR_NOT = "not";
    public final static String PR_READ = "read";
    public final static String PR_WRITE = "write";
    public final static String OP_MENOR = "<";
    public final static String OP_MENOR_IGUAL = "<=";
    public final static String OP_MAYOR = ">";
    public final static String OP_MAYOR_IGUAL = ">=";
    public final static String OP_DISTINTO = "<>";
    public final static String OP_IGUAL = "=";
    public final static String OP_SUMA = "+";
    public final static String OP_RESTA = "-";
    public final static String OP_MULTIPLICACION = "*";
    public final static String OP_DIVISION = "/";
    public final static String OP_ASIGNACION = ":=";
    public final static String OP_PUNTO = ".";
    public final static String OP_COMA = ",";
    public final static String OP_PUNTO_Y_COMA = ";";
    public final static String OP_DOS_PUNTOS = ":";
    public final static String OP_PARENTESIS_QUE_ABRE = "(";
    public final static String OP_PARENTESIS_QUE_CIERRA = ")";
}
