package util;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;

public class EscritorCodigoMepa {

    File archivo;
    BufferedWriter bw;

    public EscritorCodigoMepa(String nombre) {
        if (nombre.contains(".")) {
            nombre = nombre.substring(0, nombre.lastIndexOf("."));
        }
        archivo = new File(nombre + ".mep");
    }

    public void escribirFuente(String fuente) {
        try {
            bw = new BufferedWriter(new FileWriter(archivo));
            bw.write(fuente);
            bw.close();
        } catch (IOException ex) {
            Logger.getLogger(EscritorCodigoMepa.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
