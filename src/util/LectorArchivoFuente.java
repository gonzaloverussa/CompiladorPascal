package util;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import static errores.Error.errorLexico;

public class LectorArchivoFuente {

    private static int lineas = 0;
    private String dire;
    private final FileReader file;
    private final BufferedReader reader;

    public LectorArchivoFuente(String archivo) throws FileNotFoundException {
        dire = new File(archivo).getAbsolutePath();
        file = new FileReader(dire /*+ "/" + archivo*/);

        reader = new BufferedReader(file);
    }

    public String obtenerDireccionAbsoluta() {
        return dire;
    }

    public String retornarLinea() {
        String respuesta = "Fin de archivo";
        try {
            if ((respuesta = reader.readLine()) != null) {
                lineas++;
            } else {
                reader.close();
            }
        } catch (IOException ex) {
            errorLexico("Se produjo un error con la lectura del archivo. Verifique que no este dañado.");
        }
        return respuesta;
    }

    public static int numeroLinea() {
        return lineas;
    }
}
